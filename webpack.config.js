const path = require('path');
const VueLoaderPlugin = require('vue-loader/lib/plugin');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

module.exports = {
	mode: "production",
	entry: ["./src/scss/styles.scss", "./src/app.js"],
	module: {
		rules: [
			{
				test: /\.vue$/,
				use: ['vue-loader']
			},
			{
				test: /\.(css|scss)$/,
				use: [
					'vue-style-loader',
					MiniCssExtractPlugin.loader,
					'css-loader',
					{
						loader: "sass-loader",
						options: {
							sourceMap: true,
							data: `@import "./src/scss/colors.scss";`
						}
					}
				]
			},
		]
	},
	plugins: [
		new VueLoaderPlugin(),
		new MiniCssExtractPlugin({
			filename: "app.css"
		})
	],
	output: {
		filename: "app.js",
		path: path.resolve(__dirname, 'public/dist')
	},
	resolve: {
		extensions: ['.vue', '.js', '.json'],
		alias: {
			'vue$': 'vue/dist/vue.esm.js'
		}
	},
}
