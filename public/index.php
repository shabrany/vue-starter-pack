<!doctype html>

<html lang="en">
<head>
	<meta charset="utf-8">

	<title>The HTML5 Herald</title>
	<meta name="description" content="The HTML5 Herald">
	<meta name="author" content="SitePoint">
	<link rel="stylesheet" href="dist/app.css">
</head>

<body>

	<div id="app">
		<h1>Hello Vue & Webpack!</h1>
		<todo-list></todo-list>
	</div>
	<script src="dist/app.js"></script>
</body>
</html>
