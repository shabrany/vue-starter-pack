import Vue from 'vue';
import TodoList from './js/TodoList.vue';

Vue.component('todo-list', TodoList);

new Vue({
	el: '#app'
});
